import getpass, os
# getpass is used to get current user
print('Current User: ' + getpass.getuser())
# the os.environ is a variable that contains environment variables
print('PATH: ' + os.environ['PATH'])
# __file__ is the full path to the current file, os.path.basename removes the path and just gives the filename
print('Current Script: ' + os.path.basename(__file__))