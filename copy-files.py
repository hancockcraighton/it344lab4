import sys, shutil

# check for 3 because sys.argv[0] is the script name
if (len(sys.argv) != 3):
    # tell user their mistake
    exit(1,'invalid parameters; requires [from] [to]')
# gather directory arguments
fromDir = sys.argv[1]
toDir = sys.argv[2]
# do the copying
shutil.copytree(fromDir, toDir)